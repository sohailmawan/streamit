//For Responsive Carousel Slider
$(document).ready(function(){
    $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        responsiveClass:true,
        responsive:{
            480:{
                items:3,
                nav:false
            },
            576:{
                items:3,
                nav:true
            },
            768:{
                items:4,
                nav:true
            },
            992:{
                items:5,
                nav:true,
                loop:false
            },
            1200:{
                items:6,
                nav:true,
                loop:false
            },
            1400:{
                items:9,
                nav:true,
                loop:false
            }

        }
    });

    // For Sticky Movies Filters Nav
    var top_header = 60;
    console.log("window scroll:", $(window).scrollTop());
    $(window).bind('scroll', function () {
        if ($(window).scrollTop() > top_header) {
            $('.movies-filter').addClass('position-fixed');
        } else {
            $('.movies-filter').removeClass('position-fixed');
        }
    });
});